﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    [SerializeField] GameObject Cube;
 

    public float Step = 0.1f;


    void Update()
    {

        transform.position = Vector3.MoveTowards(transform.position, new Vector3(Cube.transform.position.x,gameObject.transform.position.y,gameObject.transform.position.z), Step);

    }
}
