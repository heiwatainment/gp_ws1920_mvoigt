﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float Speed = 10;

    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            gameObject.transform.position += new Vector3(1, 0, 0) * Speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.A))
        {
            gameObject.transform.position -= new Vector3(1, 0, 0) * Speed * Time.deltaTime;
        }
    }
}
